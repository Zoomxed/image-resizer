import Vue from 'vue'
import App from './App'
import router from './router'
import './axios'

Vue.config.productionTip = false

const DEFAULT_TITLE = 'Image resizer'
router.afterEach((to, from) => {
  Vue.nextTick(() => {
    document.title = to.meta.title || DEFAULT_TITLE
  })
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
