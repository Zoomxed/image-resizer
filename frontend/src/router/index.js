import Vue from 'vue'
import Router from 'vue-router'
import BootstrapVue from 'bootstrap-vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import Home from '@/views/Home'
import About from '@/views/About'

Vue.use(Router)
Vue.use(BootstrapVue)
Vue.use(VueAxios, axios)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
      meta: {
        title: 'Home page'
      }
    },
    {
      path: '/about',
      name: 'About',
      component: About,
      meta: {
        title: 'About page'
      }
    },
    {
      path: '*',
      name: 'NotFound',
      component: () => import('@/views/NotFound'),
      meta: {
        title: 'Error'
      }
    }
  ],
  mode: 'history'
})
