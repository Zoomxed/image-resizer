import logging
from flask import Flask
from app.config import configs
from flask_cors import CORS
from app import tasks


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def create_worker():
    return tasks.celery


def create_app(config_name: str = 'development'):
    """
    Flask app factory - instantiates Flask app with given config
    (Development by default

    :param config_name: Name of config for Flask app
    :return: Flask app instance
    """
    app = Flask(
      __name__,
      static_folder='../dist/static',
      template_folder="../dist"
    )
    app.config.from_object(configs[config_name])
    cors = CORS(app, resources={r'/api/*': {'origins': '*'}})

    app.logger.info(f'Flask instance created with {config_name}')

    # Register blueprints
    from .api import api_bp
    from app.api.controller import front_bp
    app.register_blueprint(api_bp, url_prefix='/api')
    app.register_blueprint(front_bp, url_prefix='/')

    app.logger.info('Blueprints successfully registered')

    return app


def register_extensions(app):
    # Register flask extensions
    pass
