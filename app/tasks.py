import os
from celery.utils.log import get_task_logger
from PIL import Image
from celery import Celery
from flask import current_app

from dotenv import load_dotenv
load_dotenv()

logger = get_task_logger(__name__)

CELERY_BROKER_URL = os.environ.get('CELERY_BROKER_URL', 'redis://localhost:6379'),
CELERY_RESULT_BACKEND = os.environ.get('CELERY_RESULT_BACKEND', 'redis://localhost:6379')
celery = Celery(__name__, broker=CELERY_BROKER_URL, backend=CELERY_RESULT_BACKEND)


@celery.task(bind=True, name='tasks.resize_image')
def resize_image(self, path: str, filename: str, width: int, height: int) -> str:
    logger.info(f'Start resizing to ({width},{height})')

    with Image.open(path) as img:
        resized_img = img.resize((width, height), resample=Image.NEAREST)
        resized_img.save(path)

    logger.info('Resizing completed')

    path_to_url = os.path.join('static/images', filename)
    return str(path_to_url)
