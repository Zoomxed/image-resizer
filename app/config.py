import os
basedir = os.path.abspath(os.path.dirname(__file__))
workdir = os.getcwd()
static_folder = 'dist/static'


class BaseConfig:
    DEBUG = False
    APP_NAME = os.environ.get('APP_NAME', 'Image-resizer-service')
    SECRET_KEY = os.environ.get('SECRET_KEY', os.urandom(24))

    REDIS_HOST = os.environ.get('REDIS_URL', 'localhost')
    REDIS_PORT = os.environ.get('REDIS_PORT', '6379')
    REDIS_PASSWORD = os.environ.get('REDIS_PASSWORD', None)

    # Upload
    UPLOAD_DIR = os.path.join(workdir, static_folder, 'images')
    ALLOWED_EXTENSIONS = {'jpg', 'png', 'jpeg'}


class DevelopementConfig(BaseConfig):
    DEBUG = True


class TestingConfig(BaseConfig):
    DEBUG = True
    TESTING = True

    # fixture images
    TEST_DIR = os.path.join(workdir, 'images')
    TEST_IMAGE = os.path.join(TEST_DIR, 'test.jpg')


class ProductionConfig(BaseConfig):
    DEBUG = False


configs = {
    'development': DevelopementConfig,
    'testing': TestingConfig,
    'production': ProductionConfig
}
