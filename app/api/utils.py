from flask import current_app


def allowed_extension(content_type: str) -> bool:
    """
    Checks if extension of given filename in allowed extensions set
    :param content_type: Type to check
    :return: Is valid
    """
    return content_type.split('/')[1] in current_app.config.get('ALLOWED_EXTENSIONS')


def valid_size(size: tuple) -> bool:
    """
    Validates dict with height and width
    :return: Is valid
    """
    w, h = map(int, size)
    if 0 < w < 10000 and 0 < h < 10000:
        return True

    return False
