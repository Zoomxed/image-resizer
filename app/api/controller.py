import json
import kombu
from flask_restx import Resource, reqparse, abort
from app.api.dto import ResizeDto
from app.api.utils import allowed_extension, valid_size
from werkzeug.datastructures import FileStorage
from werkzeug.utils import secure_filenam
from flask import current_app, Blueprint, render_template
import os
from uuid import uuid4
from app import tasks
from app.tasks import celery


front_bp = Blueprint('front', __name__)
api = ResizeDto.api

upload_parser = reqparse.RequestParser()
upload_parser.add_argument('image_data', type=FileStorage,
                           location='files', required=True)
upload_parser.add_argument('size_target', type=str, required=True)



@api.route('/resize/')
class ResizeImageTask(Resource):
    @api.expect(upload_parser)
    def post(self):
        args = upload_parser.parse_args()
        file = args['image_data']
        target_size = json.loads(args['size_target'])
        width = int(target_size['width'])
        height = int(target_size['height'])
        print(width, height)

        if file.filename == '':
            abort(422, 'No file given')

        if not valid_size((width, height)):
            abort(422, 'Invalid size')

        if file and allowed_extension(file.content_type):
            task_id = str(uuid4())
            extension = file.filename.rsplit('.', 1)[1]
            filename = secure_filename(f'{task_id}.{extension}')
            path = os.path.join(current_app.config['UPLOAD_DIR'], filename)
            file.save(path)

            try:
                task = tasks.resize_image.apply_async(
                    args=[path, filename, width, height],
                    task_id=task_id
                )

            except kombu.exceptions.OperationalError as e:
                return abort(500, f'Server error: {e}')

            return {'task_id': task.id}, 202
        else:
            abort(422, 'Not allowed extension')


@api.route('/task/<uuid:task_id>')
class GetStatusTask(Resource):
    def get(self, task_id):
        current_app.logger.info(f'get task result with id{task_id}')
        state = get_status(str(task_id))
        if state['status'] == 'SUCCESS':
            return {
                'status': state.get('status'),
                'url_image': state.get('url_image'),
            }, 200
        elif state['status'] == 'PENDING':
            return {
                'status': state.get('status'),
                'url_image': state.get('url_image'),
            }, 206


def get_status(task_id):
    try:
        task = celery.AsyncResult(task_id)
        current_app.logger.info(f'Get task id {task_id}')
    except AttributeError as err:
        current_app.logger.critical(f'Celery backend disabled: {err}')

    state = {
        'status': task.status,
        'url_image': task.result
    }
    return state


@front_bp.route('/', defaults={'path': ''})
@front_bp.route('/<path:path>')
def catch_all(path):
    return render_template("index.html")
