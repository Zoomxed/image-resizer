from flask_restx import Namespace, fields
from werkzeug.datastructures import FileStorage


class ResizeDto:

    api = Namespace('image', description='Resize related operations.')

    resize_model = api.model(
        'Resize images', {
            'imageData': FileStorage,
            'widthTarget': fields.Integer(required=True, description='Target width'),
            'heightTarget': fields.Integer(required=True, description='Target height')
        },
    )
