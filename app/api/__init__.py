from flask_restx import Api
from flask import Blueprint
from app.api.controller import api as api_ns


api_bp = Blueprint('api', __name__)


api = Api(api_bp, version='1.0', title='Resize api',
          description='API for async resize images')

api.add_namespace(api_ns)
